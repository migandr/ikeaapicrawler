## Definition of the fields that are in process for IKEA

- **ItemNo**: product number, when the article is a combination of products, it is preceded with an 'S'.
- **ProductName**: the name of the product.
- **ProductType**: type of the product
- **OnlineSellable**: is this article available for online selling, this field does not mean that there is stock for
online selling.
- **BreathTakingItem**: article that has a special low price.
- **RetailPriceType**: define if the price is regular price or offer price.
- **Price**: sale price with taxes.
- **PriceExclTax**: sale price without taxes.
- **ItemColour**: COLOUR
- **ItemSize**: SIZE
- **ProductDescription**: the SEO description of the article  
- **PackageWidth**: Width
- **PackageHeight**: Height
- **PackageLength**: Length
- **PackageWeight**: Weight
- **ProductDepartment**: associated department for the product.
- **ProductSeasonality**: if the product is seasonal, displays this seasonal
- **ItemAverageRating**: final rating of the product.
- **ItemCountRating**: number of ratings of the product
