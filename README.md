# IAC

## :mag_right: Description

Build over Python, IAC is created to extract information of current existing articles to feed a database that helps in
making stock predictions using time series analysis. The current version is extracting information from the TOP 100
articles based on number of ratings and average of those ratings.

IAC is composed by fifth modules, fourth of them are available in this repository:

- data_extractor
- stock_availability
- weather
- notifications

The fifth is a notebook not available in this repository:

- top_100_extractor

**1. data_extractor**
---

This tool extracts some basic information from the articles using one of the available IKEA APIs. This information is
defined on the `data_governance.md` file.

**2. stock_availability**
---

This tool executes the query to extract the current stock availability for the top 100 articles of the list in the
stores of Madrid and Barcelona.

**3. weather**
---

This tool extracts the weather conditions from the 6 stores that are under analysis, in order to perform it you have 
to follow the steps defined in **configure AEMET API** section.

**4. notifications**
---

This tool sends a message with the notification of a proper end of process (Still in Test)

**5. top_100_extractor**
---

A jupyter notebook that clean data and extracts the reference number of the top100 articles required for the next
step.

The reasons why this notebook is not in this repository are:

- This a temporary functionality.
- ipynb files always creates a lot of changes in the commits when they are executed.

## :twisted_rightwards_arrows: Architecture

This tool is build using Python 3.6 and Python3.8, it is not a problem due to all modules are compatible between them.

The logs file are prepared to be consumed with a monitorizing tool. In this case, it is a setup created with:

- Elasticsearch`
- Kibana
- Filebeat


## :key: Configure AEMET API

In order to make **weather** work you have to create your own API key at the [AEMET OpenData Website](https://opendata.aemet.es/centrodedescargas/inicio)
and then store it in a file at weather/config/api_key.py` under the following format:

```python

API_KEY = "xxxxxxxx"

```

## :key: Configure TWITTER API

In order to make **twitter notifications** work you have to create your own API key at the
[Twitter Developers Portal](https://developer.twitter.com/), request permissions for direct messages
and then store the keys in a file at notifications/configuration/config.py` under the following format:

```python

CONSUMER_KEY = "XXXXXXXX"
CONSUMER_SECRET = "XXXXXXXX"
ACCESS_TOKEN = "XXXXXXX-XXXXXXXXXX"
ACCESS_TOKEN_SECRET = "XXXXXXXXX"
RECIPIENT_ID = "XXXXXXXXXX"

```

## :rocket: Execution

To execute data_extractor:

```bash
  
  python3 -m data_extractor
  
```

To execute stock_availability:

```bash
  
  python3 -m stock_availability
  
```

To execute weather:

```bash

  python3 -m weather


```

## :fire: Dependencies

To install the dependencies of this project, execute `pip3 install -r requirements.txt`

- **joblib** version 0.14.1
- **tqdm** version 4.33.0
- **requests** version 2.22.0
- **beautifulsoup4** version 4.9.3
- **tweepy** version 3.9.0

## :sweat_smile: About the developer

This tool has been developed by Miguel Andreu.

I'm an spanish Software Developer and Data Engineer, currently Studying the MSC in Big Data,
Data Science and Data Engineering at "Univerdad Autónoma de Madrid" and workin as Observability Engineer. 

:iphone: (Teams) m.andreunieva@gmail.com (Miguel Andreu Nieva)

:email: m.andreunieva@gmail.com

If you have any issue or want to add any enhancement, please use the **issues tab** to request them. 

---
