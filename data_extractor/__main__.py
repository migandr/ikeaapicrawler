# !/usr/bin/Python3.7

import multiprocessing
from joblib import Parallel, delayed
import requests
import time
from tqdm import tqdm
from data_extractor.tools import app_functionalities as app
from data_extractor.tools.file_generator import file_initiator, write_error, write_log
from data_extractor.tools.name_generator import name_generator
from data_extractor.configuration import configuration as config


def basic_app(i):
    art = str()
    try:
        r = app.make_request(i)
        if r.status_code == 200:
            write_log(content=art, log_type=config.E_99_CODE, log_source=config.IKEA_API)
            app.app_status_200(art, r, file_name)
        else:
            if r.status_code == 403:
                write_log(content=art, log_type=config.E_403_LOG_CODE, log_source=config.IKEA_API)
                write_error(file_name=config.ERROR_FILE, content=str(config.E_403_MESSAGE) + str(i))
                time.sleep(301)
            else:
                print(r.status_code)
    except (Exception, BaseException):
        write_log(content=art, log_type=config.E_GEN_CODE, log_source=config.IKEA_API)
        write_error(file_name=config.ERROR_FILE, content=str(config.E_GEN_MESSAGE) + str(i))
    except requests.exceptions.ConnectionError:
        write_log(content=art, log_type=config.E_BAN_CODE, log_source=config.IKEA_API)
        write_error(file_name=config.ERROR_FILE, content=str(config.E_BAN_MESSAGE) + str(i))


if __name__ == "__main__":
    file_name = name_generator(source=1)
    file_initiator(file_name=file_name)
    file_initiator(file_name=config.ERROR_FILE)
    references = app.categories()
    Parallel(n_jobs=multiprocessing.cpu_count())(delayed(basic_app)(i) for i in tqdm(references))
