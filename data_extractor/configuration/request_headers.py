# !/usr/bin/Python3.7

ARTICLES_HEADER = {
  'Accept': 'application/vnd.ikea.iows+json;version=2.0',
  'Accept-Encoding': 'gzip, deflate, br',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:81.0) Gecko/20100101 Firefox/81.0',
  'Origin': 'https://www.ikea.com',
  'Consumer': 'MAMMUT',
  'Contract': '37249',
  'Referer': 'https://www.ikea.com/es/es/',
  'Host': 'iows.ikea.com',
  'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3'
}

CATEGORIES_HEADER = {
  'Accept': '*/*',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3',
  'Connection': 'keep-alive',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:81.0) Gecko/20100101 Firefox/81.0'
}
