# !/usr/bin/Python3.8

IKEA_API = "IKEA_API"
ERROR_FILE = "error.txt"
CATEGORIES_URL = "https://sik.search.blue.cdtapps.com/es/es/product-list-page?category={}&size=1000"
ARTICLES_URL = "https://iows.ikea.com/retail/iows/es/es/catalog/items/{},{}?ignoreErrors=true"
SPR = "SPR"
ART = "ART"
E_403_MESSAGE = "(IKEA_API)(403)"
E_403_LOG_CODE = 1
E_GEN_MESSAGE = "(GEN)"
E_GEN_CODE = 2
E_BAN_MESSAGE = "(BAN)"
E_BAN_CODE = 3
E_99_CODE = 99
E_0_CODE = 0
RETAIL_ITEM_COMM = "RetailItemComm"
