# !/usr/bin/Python3.7

HEADERS = 'ItemNo|ProductName|ProductType|OnlineSellable|BreathTakingItem|' \
          'RetailPriceType|Price|PriceExclTax|ItemAverageRating|ItemCountRating|ItemSize|ItemColour|' \
          'ProductDescription|ProductDepartment|ProductSeasonality\r\n'


def file_initiator(file_name: str = 'default.csv'):
    """

    Starts the file with the headers

    :param file_name: (str) the name of the file

    """
    file = open(file_name, 'a+')
    if file_name.split(".")[len(file_name.split(".")) - 1] == 'csv':
        file.write(HEADERS)
    elif file_name.split(".")[len(file_name.split(".")) - 1] == 'txt':
        pass
    file.close()


def write_line(file_name: str = 'default.csv', content: list = None):
    """

    Write each line that is provided as list from the tool

    :param file_name: (str) the name of the file, opened by csv_initiator

    :param content: (list) parameters in a list that must be added as a csv row

    """

    failing = ["324518", "9850874", "389072", "103343", "263850", "268876", "378399", "361049", "309829"]

    if content[2] in failing:
        print("in function ", content[2])

    file = open(file_name, 'a+')
    line = '|'.join(map(str, content))
    file.write(line + "\r\n")
    file.close()


def write_error(file_name: str = 'error.txt', content: str = "0"):
    """

    Writes comma separated, each reference that has triggered and error

    :param file_name: (str) the file name to write
    :param content: (int) the reference number to be appended

    """
    print("write_error()")
    file = open(file_name, 'a+')
    file.writelines(str(content) + ",")
    file.close()


def write_log(file_name: str = "result.log", content: str = "fail", log_type: int = 0, log_source: str = None):
    file = open(file_name, 'a+')
    if log_type == 0:
        file.write("[{}] SUCCESS: ".format(log_source) + str(content) + "\r\n")
    elif log_type == 1:
        file.write("[{}] 403: ".format(log_source) + str(content) + "\r\n")
    elif log_type == 2:
        file.write("[{}] GEN: ".format(log_source) + str(content) + "\r\n")
    elif log_type == 3:
        file.write("[{}] CONN: ".format(log_source) + str(content) + "\r\n")
    elif log_type == 99:
        file.write("[{}] EXECUTED: ".format(log_source) + str(content) + "\r\n")
    file.close()
