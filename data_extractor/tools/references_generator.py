# !/usr/bin/Python3.7


def references_generator(value: str) -> str:
    """

    Generates reference numbers of length equal to 9 and filled from the left with 0

    :param value: (str) number to be referenced

    :return: (str) reference number of length 9

    """
    ref_str = str(value)
    if len(ref_str) < 8:
        ref_str = (8 - len(ref_str)) * '0' + ref_str
    return ref_str
