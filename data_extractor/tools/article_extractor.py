# !/usr/bin/Python3.7

BASIC_DATA = ['ItemNo', 'ProductName', 'ProductTypeName', 'OnlineSellable', 'BreathTakingItem']


def dictionary_extractor(article_dictionary: dict) -> list:
    """

    Extracts from the json result of the query the defined information and append it into a list that will be returned

    :param article_dictionary: (dict) response from GET request

    :return: (list) a list with the expected parameters

    """

    article = list()
    article = extract_basic_articles(article_dictionary, article)
    article = extract_price_information(article_dictionary, article)
    article = extract_ratings(article_dictionary, article)
    article = extract_selection_criteria(article_dictionary, article)
    article = extract_product_description(article_dictionary, article)
    # article = extract_package_meassures(article_dictionary, article)
    article = extract_product_category_department(article_dictionary, article)
    return article


#
#   Basic information area, composed by
#       extract_basic_articles
#
def extract_basic_articles(article_dictionary: dict, article: list) -> list:
    """

    Extracts the basic information from the article, all this fields have in common that they are directly accessible
    from the first level of the dictionary.

    :param article_dictionary: (dict) dictionary with the information of the article
    :param article: (list) list to append

    :return: (list) list with the information appended

    """
    for dk in BASIC_DATA:
        if dk in article_dictionary.keys():
            article.append(article_dictionary[dk]['$'])
        else:
            article.append("")
    return article


#
#    Price extraction area, composed by:
#        extract_price_information
#        __check_price_attribute
#
def extract_price_information(article_dictionary: dict, article: list) -> list:
    """

    From the category RetailItemCommPriceList extract the fields regarding price type and price with and without taxes.
    When the data doesn't exist, a null for each field is returned

    :param article_dictionary: (dict) dictionary with the information of the article
    :param article: (list) list to append

    :return: (list) list with the information appended

    """
    if 'RetailItemCommPriceList' in article_dictionary.keys():
        if 'RetailItemCommPrice' in article_dictionary['RetailItemCommPriceList'].keys():
            if 'Price' in article_dictionary['RetailItemCommPriceList']['RetailItemCommPrice']:
                short_dictionary = article_dictionary['RetailItemCommPriceList']['RetailItemCommPrice']
                article.append(__check_price_attribute__('RetailPriceType', short_dictionary))
                article.append(__check_price_attribute__('Price', short_dictionary))
                article.append(__check_price_attribute__('PriceExclTax', short_dictionary))
                return article
    return append_empty_elements(3, article)


def __check_price_attribute__(expected_key: str, article_dictionary: dict) -> str:
    """

    Extract the content for each key that must be extracted from the price category.

    :param expected_key: (str) the key to be looked for
    :param article_dictionary: (dict) the article dictionary to search

    :return: (str) the value

    """
    if expected_key in article_dictionary.keys():
        return article_dictionary[expected_key]['$']
    else:
        return ""
    pass


#
#   Colour and Size basic extraction area, composed by:
#       extract_selection_criteria
#

def extract_selection_criteria(article_dictionary: dict, article: list) -> list:
    """

    From the GPRCommSelectionCriteriaSelectionList extract the information regarding the article requested. When the
    data doesn't exist, a null for each field is returned

    :param article_dictionary: (dict) dictionary with the information of the article
    :param article: (list) list to append

    :return: (list) list with the information appended

    """
    try:
        if 'GPRCommSelectionCriteriaSelectionList' in article_dictionary.keys():
            if 'GPRCommSelectionCriteriaSelection' in article_dictionary['GPRCommSelectionCriteriaSelectionList'].keys():
                criterias = article_dictionary['GPRCommSelectionCriteriaSelectionList']['GPRCommSelectionCriteriaSelection']
                size, colour = "", ""
                for criteria in criterias:
                    if criteria['SelectionCriteriaCode']['$'] == "SIZE" and size == "":
                        size = criteria['SelectionCriteriaValue']['$']
                    elif criteria['SelectionCriteriaCode']['$'] == 'COLOUR' and colour == "":
                        colour = criteria['SelectionCriteriaValue']['$']
                article.append(size)
                article.append(colour)
                return article
        return append_empty_elements(2, article)
    except TypeError:
        return append_empty_elements(2, article)


#
#   Product description extraction area, composed by:
#       extract_product_description
#
def extract_product_description(article_dictionary: dict, article: list) -> list:
    """

    From the AttributeGroupList extract the SEO Product description of the article requested. When the data doesn't
    exist, a null is returned

    :param article_dictionary: (dict) dictionary with the information of the article
    :param article: (list) list to append

    :return: (list) list with the information appended

    """
    if 'AttributeGroupList' in article_dictionary.keys():
        if 'AttributeGroup' in article_dictionary['AttributeGroupList'].keys():
            if 'AttributeList' in article_dictionary['AttributeGroupList']['AttributeGroup'].keys():
                short_dictionary = article_dictionary['AttributeGroupList']['AttributeGroup']['AttributeList']
                if 'Attribute' in short_dictionary.keys():
                    article.append(short_dictionary['Attribute']['Value']['$'])
                    return article
    return append_empty_elements(1, article)


#
#   Product meassures area, composed by
#       extract_package_meassures
#
def extract_package_meassures(article_dictionary: dict, article: list) -> list:
    """

    From the RetailItemCommPackageMeasureList extract the package meassures of the article requested. When the data
    doesn't exist, a null for each expected element is returned

    :param article_dictionary: (dict) dictionary with the information of the article
    :param article: (list) list to append

    :return: (list) list with the information appended

    """
    if 'RetailItemCommPackageMeasureList' in article_dictionary.keys():
        if 'RetailItemCommPackageMeasure' in article_dictionary['RetailItemCommPackageMeasureList'].keys():
            meassures = article_dictionary['RetailItemCommPackageMeasureList']['RetailItemCommPackageMeasure']
            for meassure in meassures:
                article.append(meassure['PackageMeasureTextMetric']['$'])
            if len(article) < 4:
                append_empty_elements(4 - len(article), article)
            return article
    return append_empty_elements(4, article)


#
#   Categorization area, composed by
#       extract_product_category_department
#       __category_department_verifier__
#       __category_department_information_extractor__
#
def extract_product_category_department(article_dictionary: dict, article: list) -> list:
    """

    For the different possible fields at 'CatalogRefList' extract the information normalized to be handled. If any
    field is empty it will be substituted by a Null value. When the data
    doesn't exist, a null for each expected element is returned

    :param article_dictionary: (dict) dictionary with the information of the article
    :param article: (list) list to append

    :return: (list) list with the information appended

    """
    if 'CatalogRefList' in article_dictionary.keys():
        if 'CatalogRef' in article_dictionary['CatalogRefList'].keys():
            catalog = article_dictionary['CatalogRefList']['CatalogRef']
            article = __category_department_verifier__(catalog, article)
            return article
    return append_empty_elements(2, article)


def __category_department_verifier__(catalog: dict, article: list) -> list:
    """

    Handles the validation of the defined possible values for this field, and handles if they are present or not,
    filling then the field with a null value.

    When the field is in the dictionary, a functionality that handles the possible values is triggered.

    :param catalog: (dict) a shorter dictionary with the article
    :param article: (list) the article list to append information

    :return: (list) the updated article list

    """
    categories = ['departments', 'seasonal']
    temp_list = list()
    for raw_element in enumerate(catalog):
        if len(raw_element) >= 1:
            for element in raw_element:
                if isinstance(element, dict):
                    temp_list.append(__category_department_information_extractor__(element))
    temp_list = __validate_list__(temp_list)
    for temp_element in temp_list:
        article.append(temp_element)
    return article


def __category_department_information_extractor__(element: dict) -> str:
    """

    Compares the expected category with each of the possible values and, when a match is performed, executes the data
    extraction.

    :param element: (dict) a even more shorter dictionary

    :return: (str) the extracted value.

    """
    if element['Catalog']['CatalogId']['$'] == 'departments':
        if 'CatalogElementList' in element.keys():
            if 'CatalogElement' in element['CatalogElementList'].keys():
                if isinstance(element['CatalogElementList']['CatalogElement'], dict):
                    if 'CatalogElementName' in element['CatalogElementList']['CatalogElement'].keys():
                        a = element['CatalogElementList']['CatalogElement']['CatalogElementName']['$']
                    else:
                        a = ""
                    if 'Catalog' in element.keys():
                        if 'CatalogName' in element['Catalog'].keys():
                            b = element['Catalog']['CatalogName']['$']
                        else:
                            b = ""
                    else:
                        b = ""
                    return str(b) + " - " + str(a)
                else:
                    short_dictionary = element['CatalogElementList']['CatalogElement']
                    values_list = list()
                    for e in short_dictionary:
                        if 'CatalogElementName' in e.keys():
                            values_list.append(e['CatalogElementName']['$'])
                        else:
                            values_list.append("")
                    values_list.reverse()
                    return " - ".join(values_list)
    elif element['Catalog']['CatalogId']['$'] == 'seasonal':
        try:
            url = str(element['CatalogElementList']['CatalogElement']['CatalogElementUrl']['$']).split("/")
            next_part = False
            for part in url:
                if next_part:
                    return part
                if part == 'seasonal':
                    next_part = True
                    continue
        except TypeError:
            return ""


#
#   Ratings area, composed by
#       extract_ratings
#
def extract_ratings(article_dictionary: dict, article: list) -> list:
    if 'RetailItemRating' in article_dictionary.keys():
        if 'ItemRating' in article_dictionary['RetailItemRating'].keys():
            if 'AverageRating' in article_dictionary['RetailItemRating']['ItemRating'].keys():
                article.append(article_dictionary['RetailItemRating']['ItemRating']['AverageRating']['$'])
            else:
                article.append(0)
            if 'Count' in article_dictionary['RetailItemRating']['ItemRating'].keys():
                article.append(article_dictionary['RetailItemRating']['ItemRating']['Count']['$'])
            else:
                article.append(0)
            return article
    return append_empty_elements(2, article)


def __validate_list__(temp_list: list) -> list:
    """

    Checks if this list has the number of parameters required to avoid errors when loading the csv.
    This information is the less valuable of all the process and it can be empty without any impact.

    :param temp_list: (list) list with the elements extracted form categories.

    :return: (list) a list with the adequated length

    """
    control = 0
    while control <= len(temp_list):
        try:
            temp_list.remove(None)
        except ValueError:
            pass
        finally:
            control += 1

    if len(temp_list) > 2:
        temp_list.pop(len(temp_list) - 1)
    elif len(temp_list) < 2:
        append_empty_elements(2 - len(temp_list), temp_list)
    return temp_list


def append_empty_elements(num_of_elements: int, article: list) -> list:
    """

    When all the articles of an 'area' fails this function append to the article (list) as much empty elements as
    provided in the parameter num_of_elements

    :param num_of_elements: (int) num of 'null' elements to be added
    :param article: (list) list to append

    :return: (list) list with the null parameters appended.

    """
    for e in range(num_of_elements):
        article.append("")
    return article
