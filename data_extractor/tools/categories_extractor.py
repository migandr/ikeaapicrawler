# !/usr/bin/Python3.8

from bs4 import BeautifulSoup
from requests_html import HTMLSession


def categories_extractor() -> list:
    """

    Extracts the existing categories from the website menu using beautiful soup.

    :return:

    """
    print("Extracting Categories")
    target = "https://www.ikea.com/es/es/"
    session = HTMLSession()
    total_pages = 0
    total_pages_list = list()
    while total_pages < 450:
        resp = session.get(target)
        resp.html.render()
        left_menu_links = BeautifulSoup(resp.html.html, "lxml").find("aside").find_all("a")
        resp.close()
        total_pages_list = [extract_info(link['href']) for link in left_menu_links if __urls_validation__(link['href'])]
        total_pages = len(total_pages_list)  # Set length into total_pages for validation
    return total_pages_list


def extract_info(href: str) -> str:
    """

    Extract the latest string, composed by 5 digits alfanumeric, from each url.

    :param href:(str) Url to be normalized

    :return: (str) normalized url

    """
    split = href.rsplit('-', 1)
    href = split[len(split) - 1]
    return str(href.replace("/", ""))


def __urls_validation__(url: str) -> bool:
    """

    Validates that the URL is related to a product page.

    :param url: (str) url to be checked.

    :return: (bool) true if the URL is related to product.

    """
    if "https://www.ikea.com/es/es/cat" in url:
        return True if url != "https://www.ikea.com/es/es/cat/productos-products/" else False

