# !/usr/bin/Python3.7
from datetime import datetime


def name_generator(source: int) -> str:
    """

    Generates a name for the file with the current date and start hour

    :param source: (str)

    :return: (str) file name

    """
    date_prefix = datetime.now().strftime('%Y_%m_%d')
    if source == 1:
        return date_prefix + str('_ikea_articles.csv')
    elif source == 2:
        return date_prefix + str('_ikea_stock.csv')
    elif source == 3:
        return date_prefix + str('_weather.csv')
