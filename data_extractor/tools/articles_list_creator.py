# !/usr(bin/Python3.8


def article_ref_extractor(category_info: dict):
    articles_list = list()
    if "productListPage" in category_info.keys():
        if "productWindow" in category_info["productListPage"].keys():
            articles = category_info["productListPage"]["productWindow"]
            if isinstance(articles, list):
                for article in articles:
                    if 'gprDescription' in article.keys():
                        if 'variants' in article['gprDescription'].keys():
                            variants = article['gprDescription']['variants']
                            for variant in variants:
                                articles_list.append(variant['id'])
                    articles_list.append(article["id"])
    return articles_list
