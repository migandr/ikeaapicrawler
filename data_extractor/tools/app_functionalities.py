# !/usr/bin/Python3.8

import requests
import json
from tqdm import tqdm
from data_extractor.tools.categories_extractor import categories_extractor
from data_extractor.tools.references_generator import references_generator
from data_extractor.tools.article_extractor import dictionary_extractor
from data_extractor.tools.file_generator import write_line, write_log
from data_extractor.configuration.request_headers import ARTICLES_HEADER, CATEGORIES_HEADER
from data_extractor.configuration import configuration as config
from data_extractor.tools.articles_list_creator import article_ref_extractor


def categories() -> list:
    """

    Wraps the extraction of the existing categories by scrapping the menu of the IKEA website, and the articles
    reference extraction using the product list pages API endpoint.

    :return: (list) a list with all the references

    """
    list_of_categories = categories_extractor()
    list_of_references = list()
    for category in tqdm(list_of_categories):
        url = config.CATEGORIES_URL.format(category)
        r = requests.get(url, headers=CATEGORIES_HEADER)
        if r.status_code == 200:
            result = json.loads(r.content)
            list_of_references.append(article_ref_extractor(result))
    return [item for sublist in list_of_references for item in sublist]


def make_request(article_number: str) -> requests:
    """

    Wraps the requests functionality deciding which kind of requests must be done.

    :param article_number: (str)

    :return: (requests) a requests object with the response

    """
    if str(article_number[0]).lower() == "s":
        art = article_number[1:]
        url = config.ARTICLES_URL.format(config.SPR, art)
    else:
        art = references_generator(article_number)
        url = config.ARTICLES_URL.format(config.ART, art)
    return requests.get(url, headers=ARTICLES_HEADER)


def app_status_200(art: str, r: requests, file_name: str, test: bool = False):
    """

    Trigger the data extraction functionality when the response of the api is a 200 status.

    :param art: (str) article number for logs
    :param r: (requests) request information to be loaded in json
    :param file_name: (str) file to write the result
    :param test: (bool) enable or disable logging

    """
    result = json.loads(r.content)
    if config.RETAIL_ITEM_COMM in result.keys():
        if not test:
            write_log(content=art, log_type=config.E_0_CODE, log_source=config.IKEA_API)
        article = dictionary_extractor(result[config.RETAIL_ITEM_COMM])
        write_line(file_name=file_name, content=article)
