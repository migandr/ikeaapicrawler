# !/usr/bin/Python3.6

import multiprocessing
from joblib import Parallel, delayed
from tqdm import tqdm
import time
from stock_availability.tools.csv_to_list import csv_to_list
from stock_availability.tools import app_functionalities as app
from stock_availability.configuration import configuration as config
from data_extractor.tools.references_generator import references_generator
from data_extractor.tools.name_generator import name_generator
from data_extractor.tools.file_generator import file_initiator, write_error, write_log
from notifications.__main__ import send_notification


def basic_app(art: str):
    art = references_generator(art)
    for store in config.STORES:
        for q_type in config.ARTICLE_TYPE:
            r = app.make_request(store, q_type, art)
            write_log(content=art, log_type=99, log_source=config.STOCK_API)
            if r.status_code == 200:
                write_log(content=art, log_type=0, log_source=config.STOCK_API)
                app.app_status_200(store, art, r, file_name)
                break
            else:
                if r.status_code == 403:
                    write_log(content=art, log_type=1, log_source=config.STOCK_API)
                    write_error(file_name='error.txt', content=str(art) + " : " + str(store))
                    time.sleep(100)
                elif r.status_code == 404:
                    pass


if __name__ == "__main__":
    file_name = "stock_output/" + config.FILE_PREFIX + name_generator(source=2)
    file_initiator(file_name='error.txt')
    articles = csv_to_list(config.TOP_100)
    Parallel(n_jobs=4)(delayed(basic_app)(i) for i in tqdm(articles))
    send_notification("Process end with no fails. Stock Updated.")
