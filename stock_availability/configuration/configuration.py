# !/usr/bin/Python3.8

ARTICLE_TYPE = ["SPR", "ART"]
STORES = ["031", "282", "498", "280", "406", "171"]
TOP_100 = './top_100.csv'
URL = "https://iows.ikea.com/retail/iows/es/es/stores/{}/availability/{}/{}"
FILE_PREFIX = 'availability_'
STOCK_API = "STOCK_API"
