# !/usr/bin/Python3.8

AVAILABILITY_HEADER = {
  'Accept': 'application/vnd.ikea.iows+json;version=1.0',
  'Accept-Encoding': 'gzip, deflate, br',
  'Accept-Language': 'es-ES,es;q=0.9',
  'Cache-Control': 'no-cache',
  'Connection': 'keep-alive',
  'Consumer': 'MAMMUT',
  'Contract': '37249',
  'Host': 'iows.ikea.com',
  'Origin': 'https://www.ikea.com',
  'Pragma': 'no-cache',
  'Referer': 'https://www.ikea.com/es/es/p/tussoy-colchoncillo-topper-confort-blanco-70298139/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36 SECSSOBrowserChrome'
}