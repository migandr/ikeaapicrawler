# !/usr(bin/Python3.8


def csv_to_list(file_name: str) -> list:
    """

    Opens a csv file and returns it as a list

    :param file_name: (str) file to be opened

    :return: (list) articles to be checked

    """
    articles = list()
    with open(file_name) as f:
        for line in f.readlines():
            articles.append(line.replace('\n', ''))
    return articles
