# !/usr/bin/Python3.8

import json
import requests
from datetime import datetime
from stock_availability.configuration.request_headers import AVAILABILITY_HEADER
from stock_availability.configuration import configuration as config
from stock_availability.tools.availability_extractor import availability_extractor
from data_extractor.tools.file_generator import write_line


def make_request(store: str, query_type: str, article_reference: str) -> requests:
    """

    Makes the requests for the given parameters:

    :param store: (str) the number of the store defined at the config file in string
    :param query_type: (str) the article type for the query (ART or SPR)
    :param article_reference: (str) the article reference

    :return: (requests) the result of the requests in raw.

    """
    url = config.URL.format(store, query_type, article_reference)
    return requests.get(url, headers=AVAILABILITY_HEADER)


def app_status_200(store: str, article_reference: str, r: requests, file_name: str):
    """

    When an request status_code is equal to 200 this functionality triggers the availability extractor function and
    appends it to a list that will be wrote in a CSV file.

    :param store: (str) the number of the store defined at the config file in string
    :param article_reference: (str) the article reference
    :param r: (request) with the data to be extracted and normalized
    :param file_name: (str) the name of the file where the data must be wrote.

    """
    article = [store, datetime.now().strftime('%Y/%m/%d'), str(article_reference).rstrip()]
    result = json.loads(r.content)
    article.append(availability_extractor(result))
    write_line(file_name=file_name, content=article)
