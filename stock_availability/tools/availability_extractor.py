# !/usr/bin/Python3.8


def availability_extractor(article: dict) -> int:
    """

    Extracts the availability of the article from the result of the requests for the current day.

    :param article: (dict) the requests information of the article

    :return: (int) the availability value.

    """
    if 'StockAvailability' in article.keys():
        if 'RetailItemAvailability' in article['StockAvailability'].keys():
            if 'AvailableStock' in article['StockAvailability']['RetailItemAvailability'].keys():
                return article['StockAvailability']['RetailItemAvailability']['AvailableStock']['$']
    return 0
