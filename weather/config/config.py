# !/usr/bin/Python3.8

URL = "https://opendata.aemet.es/opendata"
ENDPOINT = "/api/prediccion/especifica/municipio/diaria/{}"
HEADERS = {
    'cache-control': "no-cache",
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36 SECSSOBrowserChrome'
    }
LOCALITY_CODES = ["28079",
                  "28134",
                  "28007",
                  "08101",
                  "08015",
                  "08187"]
STORES = ["498", "282", "031", "406", "280", "171"]
