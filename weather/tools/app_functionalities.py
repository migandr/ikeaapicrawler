# !/usr/bin/Python3.8

import requests
import json
from weather.config import config, api_key


def make_requests(locality_code: str) -> dict:
    """

    This functionality makes the requests to the AEMET endpoint for each locality

    :param locality_code: (str) the code for the stores localities

    :return: (dict) the result normalized to dictionary

    """
    requests.packages.urllib3.disable_warnings()
    requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
    query_string = {"api_key": api_key.API_KEY}
    r = requests.get(config.URL + config.ENDPOINT.format(locality_code),
                     params=query_string,
                     headers=config.HEADERS, verify=False)
    if r.status_code == 200:
        return json.loads(r.text)
    else:
        raise requests.exceptions.BaseHTTPError


def extract_locality_information(locality_code: str) -> dict:
    """

    Wrappes and handles the operation of extraction the information from the API endpoint using the response of the
    make_requests functionality to requests the data from the second required query

    :param locality_code: (str) the code for the stores localities

    :return: (dict) the result normalized to dictionary

    """
    result = make_requests(locality_code)
    if result["descripcion"] == "exito" and result["estado"] == 200:
        r = requests.get(result["datos"])
        return json.loads(r.text)
    else:
        raise requests.exceptions.BaseHTTPError



