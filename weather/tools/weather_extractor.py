# !/usr/bin/Python3.8

from data_extractor.tools.article_extractor import append_empty_elements
from data_extractor.tools.file_generator import write_line
from datetime import datetime


def weather_extractor(result: dict, store: str, file_name: str):
    """

    Executes all the funcitonalities required to extract the following parameters for a given store:
    - Precipitation probability
    - Temperature
    This information wil be added to a CSV stored per day.

    :param result: (dict) the result of the requests normalized as dict
    :param store: (str) the store number to be added on the line
    :param file_name: (str) the name of the file where the information will bre written.

    """
    today_prediction = list()
    result: dict = result[0]
    today_prediction.append(store)
    today_prediction.append(datetime.now().strftime('%Y/%m/%d'))
    if "prediccion" in result.keys():
        if "dia" in result["prediccion"].keys():
            daily_prediction: dict = result["prediccion"]["dia"][0]
            today_prediction.append(precipitations_probability(daily_prediction))
            today_prediction = temperature(daily_prediction, today_prediction)
    if len(today_prediction) < 4:
        today_prediction = append_empty_elements(4, today_prediction)
    write_line(file_name, today_prediction)


def precipitations_probability(daily_prediction: dict) -> float:
    """

    Finds and extracts the average probability of precipitation for all the day, if the data is not available a 0 will
    be returned.

    :param daily_prediction: (dict) with the information of the current day.

    :return: (float) the value of the probability from 0 to 100

    """
    if "probPrecipitacion" in daily_prediction.keys():
        pred = daily_prediction["probPrecipitacion"][0]["value"]
        if pred != "":
            try:
                return float(pred)
            except (BaseException, Exception):
                return 0.0
    else:
        return 0.0


def temperature(daily_prediction: dict, today_prediction: list) -> list:
    """

    The expected minimum and maximum temperature for the day, if any of them, or both, is empty, a 0 will be returned.

    :param daily_prediction: (dict) with the information of the current day.
    :param today_prediction: (list) where the information must be appended

    :return: (list) updated list.

    """
    if "temperatura" in daily_prediction.keys():
        if "maxima" in daily_prediction["temperatura"].keys() and \
                "minima" in daily_prediction["temperatura"].keys():
            try:
                today_prediction.append(float(daily_prediction["temperatura"]["maxima"]))
            except (BaseException, Exception):
                today_prediction.append(0.0)
            try:
                today_prediction.append(float(daily_prediction["temperatura"]["minima"]))
            except (BaseException, Exception):
                today_prediction.append(0.0)
            return today_prediction
    return append_empty_elements(2, today_prediction)


