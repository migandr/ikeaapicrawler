# !/usr/bin/Python3.8

from weather.tools.app_functionalities import extract_locality_information
from weather.tools.weather_extractor import weather_extractor
from data_extractor.tools.name_generator import name_generator
from weather.config import config
from notifications.__main__ import send_notification


def basic_app(localities: list):
    file_name = "weather_output/" + name_generator(source=2)
    for store, locality in zip(config.STORES, localities):
        weather_extractor(extract_locality_information(locality), store, file_name)


if __name__ == "__main__":
    basic_app(config.LOCALITY_CODES)
    send_notification("Process end with no fails. Weather Updated.")
