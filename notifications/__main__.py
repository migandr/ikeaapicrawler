# !/usr/bin/Python3.8

import tweepy
from notifications.configuration import configuration as config


def send_notification(message: str):
    """

    Sends a notification when the process has finished properly via twitter.

    :param message: (str) the text to be sent

    """
    auth = tweepy.OAuthHandler(config.CONSUMER_KEY, config.CONSUMER_SECRET)
    auth.set_access_token(config.ACCESS_TOKEN, config.ACCESS_TOKEN_SECRET)

    api = tweepy.API(auth)
    text = message

    direct_message = api.send_direct_message(config.RECIPIENT_ID, text)
